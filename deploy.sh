#!/bin/bash

# Variables
ssh_pub_key1="1234"
ssh_pub_key2="4321"

# Deployment
## Updates
echo "############# Updating the system #############"
sudo apt update && sudo apt upgrade -y
sleep 5
echo ""

## Install necessary packages
echo "############# Installing packages #############"
sudo apt install unattended-upgrades apt-listchanges vim ufw -y
sleet 5
echo ""

## Firewall
echo "############# Activating the firewall #############"
sudo ufw allow from 172.16.11.0/24 to any port 22 proto tcp
sudo ufw allow from 172.16.12.0/24 to any port 22 proto tcp
sudo ufw --force enable
sudo ufw status verbose
sleep 5

## SSH keys
echo "############# Adding SSH keys to authorized_keys #############"
mkdir /home/$USER/.ssh
touch /home/$USER/.ssh/authorized_keys
echo "${ssh_pub_key1}" > /home/$USER/.ssh/authorized_keys
echo "${ssh_pub_key2}" >> /home/$USER/.ssh/authorized_keys
cat /home/$USER/.ssh/authorized_keys
sleep 5
echo ""

## SSH server
echo "############# Editing the SSH daemon file to only permit SSH keys #############"
# At the moment this will also change the content in the "PAM" section
sudo sed -i 's/^#\{0,1\}PermitRootLogin.*/PermitRootLogin no/g' /etc/ssh/sshd_config
sudo sed -i 's/^#\{0,1\}PubkeyAuthentication.*/PubkeyAuthentication yes/g' /etc/ssh/sshd_config
sudo sed -i 's/^#\{0,1\}PasswordAuthentication.*/PasswordAuthentication no/g' /etc/ssh/sshd_config
sed -n 32,40p /etc/ssh/sshd_config
sed -n 57,59p /etc/ssh/sshd_config
sleep 5
echo ""

## Automatic updates
echo "############# Use sed to change the configuration file #############"
sudo sed -i 's/.*codename}-updates.*/        "origin=Debian,codename=${distro_codename}-updates";/g' /etc/apt/apt.conf.d/50unattended-upgrades
sudo sed -i 's/.*codename},label=Debian.*/        "origin=Debian,codename=${distro_codename},label=Debian";/g' /etc/apt/apt.conf.d/50unattended-upgrades
sudo sed -i 's/.*codename},label=Debian-Security.*/        "origin=Debian,codename=${distro_codename},label=Debian-Security";/g' /etc/apt/apt.conf.d/50unattended-upgrades
sudo sed -i 's/.*codename}-security,label=Debian-Security.*/        "origin=Debian,codename=${distro_codename}-security,label=Debian-Security";/g' /etc/apt/apt.conf.d/50unattended-upgrades
sed -n 23,36p /etc/apt/apt.conf.d/50unattended-upgrades
sleep 5

### Set dpkg to low priority
sudo dpkg-reconfigure -f noninteractive -plow unattended-upgrades
sleep 5

echo ""
echo "############# Create the auto-upgrades file and specify timers #############"
sudo mv /etc/apt/apt.conf.d/20auto-upgrades /home/$USER/20auto-upgrades.bk
echo "APT::Periodic::Update-Package-Lists \"1\";" > 20auto-upgrades
echo "APT::Periodic::Unattended-Upgrade \"1\";" >> 20auto-upgrades
sudo cp 20auto-upgrades /etc/apt/apt.conf.d/20auto-upgrades
rm 20auto-upgrades
sudo chmod 644 /etc/apt/apt.conf.d/20auto-upgrades
sudo cat /etc/apt/apt.conf.d/20auto-upgrades
sleep 5

# End
echo ""
echo "############# Finished - restart in 60 seconds #############"
echo "Cancel with 'shutdown -c'"
sudo sudo shutdown -r 1
echo ""